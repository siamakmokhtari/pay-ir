﻿/* eslint-disable no-undef */
function some(array, predicate) {
	let index = -1;
	const length = array == null ? 0 : array.length;
	while (++index < length) {
		if (predicate(array[index], index, array)) {
			return true;
		}
	}
	return false;
}

function commaSeparatedNumber(val) {
	val = val.toString().replace(/,/g, "");
	for (
		var valSplit = val.split(".");
		/(\d+)(\d{3})/.test(valSplit[0].toString());

	)
		valSplit[0] = valSplit[0].toString().replace(/(\d+)(\d{3})/, "$1,$2");
	return (val =
		2 == valSplit.length ? `${valSplit[0]}.${valSplit[1]}` : valSplit[0]);
}

function ToggleClass(selector) {
	$(".plansTable .row.plan").removeClass("highlight");
	$(`.${selector}`).addClass("highlight");
}

function freezeScroll() {
	// lock scroll position, but retain settings for later
	const scrollPosition = [
		self.pageXOffset ||
			document.documentElement.scrollLeft ||
			document.body.scrollLeft,
		self.pageYOffset ||
			document.documentElement.scrollTop ||
			document.body.scrollTop
	];
	const $html = $("html"); // it would make more sense to apply this to body, but IE7 won't have that
	$html.data("scroll-position", scrollPosition);
	$html.data("previous-overflow", $html.css("overflow"));
	$html.css("overflow", "hidden");
	window.scrollTo(scrollPosition[0], scrollPosition[1]);
}

const pricing = {
	_100_200: {
		plan: "basic",
		increment: 10,
		basePrice: 0
	},
	_200_600: {
		plan: "basic",
		increment: 10,
		basePrice: 1450
	},
	_600_1000: {
		plan: "standard",
		increment: 10,
		basePrice: 1400
	},
	_1000_2000: {
		plan: "premium",
		increment: 10,
		basePrice: 1350
	},
	_2000_5000: {
		plan: "pro",
		increment: 10,
		basePrice: 1300
	},
	_5000: {
		plan: "enterprise",
		increment: 50,
		basePrice: "Contact Us"
	}
};

function calcPlan(property, base) {
	const { increment, plan, basePrice } = pricing[property];
	const curve = this;
	let numberOfTasks = 0;
	if (base) {
		numberOfTasks = Math.floor((100 * curve) / increment) * increment;
	} else {
		numberOfTasks =
			increment + Math.floor((100 * curve) / increment) * increment;
	}
	ToggleClass(plan);
	return { numberOfTasks, basePrice };
}

$(document).ready(() => {
	const $h1 = $(".tasks h1");
	const $cost = $(".cost h1");
	const $tooltip = $(".handle-tooltip p");
	const $estimator = $(".costEstimator");
	const $enterprise = $(".enterprise");

	/* Outer click to document: Close Popover */
	$(document).on("click", function(event) {
		if (
			$(event.target).closest(".popover").length !== 1 &&
			$(event.target).closest(".popover-more").length !== 1 &&
			$(event.target).closest("[data-popover]").length !== 1
		) {
			$("#rotateable-arrow").removeClass("rotated");
			$(".popover, .popover-more").removeClass("visible");
		}

		event.stopPropagation();
	});

	// Close ContactForm
	function closeContactForm() {
		$(".contactFormOverlay").removeClass("visible");
	}

	// Close ContactForm dekstop
	$(".contactFormOverlay").on("click", function(e) {
		if ($(event.target).closest(".requestInvite").length !== 1) {
			closeContactForm();
		}
	});

	// Close ContactForm mobile
	$(".mobileButton.cancel").on("click", function() {
		closeContactForm();
	});

	// Close MobileNav
	$(".mobileMenuOverlay").on("click", function(e) {
		if ($(event.target).closest(".mobileMenu").length !== 1) {
			$(".mobileMenuOverlay").removeClass("visible");
			$("html, body").css({
				overflow: "auto",
				height: "auto"
			});
		}
	});

	$("[data-popover]").on("click", function(e) {
		e.preventDefault();
		const _this = $(this);
		const $popover = $(`.${_this.data("popover")}`);
		const optionOffset = _this.data("offset");

		if ($popover.hasClass("visible")) {
			$popover.removeClass("visible");
			$(".popover, .popover-more").removeClass("visible");
			$("#rotateable-arrow").removeClass("rotated");
		} else {
			$popover.addClass("visible");

			if (!optionOffset) return; // Disable Offset for popover

			const popoverCoords = $popover.offset();
			const btnCoords = $(this).offset();
			let coords = null;

			if (optionOffset === "top") {
				coords = {
					top:
						btnCoords.top -
						($popover.outerHeight() + _this.outerHeight()),
					left:
						btnCoords.left -
						($popover.outerWidth() / 2 - _this.outerWidth() / 2)
				};
			} else if (optionOffset === "bottom") {
				coords = {
					top: btnCoords.top + _this.outerHeight() * 1.5,
					left:
						btnCoords.left -
						$(this).outerWidth() / 2 -
						$popover.outerWidth() / 2
				};
				$("#rotateable-arrow").addClass("rotated");
			}

			$popover.css({
				left: coords.left,
				top: coords.top
			});
		}
	});

	$(".mobileNav.foot").on("click", function(e) {
		const viewportOffset = $(this)
			.get(0)
			.getBoundingClientRect();

		$(".mobileMenuOverlay")
			.addClass("visible")
			.children(".mobileMenu")
			.removeClass("below above")
			.addClass("above");

		$(".mobileMenu").css({
			top: 0,
			transform: "none",
			"transform-origin": "94.2% 0% 0px"
		});

		$(".mobileMenu .list").height(viewportOffset.top - 16);
		$(".mobileMenu .arrow").css({
			bottom:
				-1 *
				(viewportOffset.top - 16 + $(".mobileMenu .arrow").height()),
			left: viewportOffset.left + viewportOffset.width / 2
		});

		freezeScroll();

		e.preventDefault();
	});

	$(".mobileNav.head").on("click", function(e) {
		const offsetTop =
			$(".siteNav").offset().top + $(".siteNav").outerHeight();

		$(".mobileMenuOverlay")
			.addClass("visible")
			.children(".mobileMenu")
			.removeClass("below above")
			.addClass("below")
			.css({
				top: offsetTop,
				transform: "none",
				"transform-origin": "94.2% 0% 0px"
			});

		$(".mobileMenu .list").height("auto");
		$(".mobileMenu .arrow").css({
			bottom: "initial",
			left: "initial"
		});

		freezeScroll();

		e.preventDefault();
	});
	const sliderChanged = value => {
		let increment;
		let numberOfTasks = 0;
		let basePrice = 0;
		let plan = "";
		const curve = Math.pow(1.2, 13 * value);

		if (curve >= 0 && curve <= 1.98) {
			// 0 < x < 200
			const plan = calcPlan.call(curve, "_100_200", true);
			numberOfTasks = plan.numberOfTasks;
			basePrice = plan.basePrice;
		} else if (curve >= 1.9 && curve <= 6) {
			// 200 < x < 600
			const plan = calcPlan.call(curve, "_200_600");
			numberOfTasks = plan.numberOfTasks;
			basePrice = plan.basePrice;
		} else if (curve >= 6 && curve <= 10) {
			// 600 < x < 1000
			const plan = calcPlan.call(curve, "_600_1000");
			numberOfTasks = plan.numberOfTasks;
			basePrice = plan.basePrice;
		} else if (curve >= 10 && curve <= 20) {
			// 1000 < x < 2000
			const plan = calcPlan.call(curve, "_1000_2000");
			numberOfTasks = plan.numberOfTasks;
			basePrice = plan.basePrice;
		} else if (curve >= 20 && curve <= 50) {
			// 2000 < x < 5000
			const plan = calcPlan.call(curve, "_2000_5000");
			numberOfTasks = plan.numberOfTasks;
			basePrice = plan.basePrice;
		} else {
			// x > 5000
			const plan = calcPlan.call(curve, "_5000");
			numberOfTasks = plan.numberOfTasks;
			basePrice = plan.basePrice;
		}

		$(".slider .handle").css("left", `${50 * value}%`);
		if (numberOfTasks >= 5000) {
			$h1.html("5,000+ AED");
			$cost.html("Contact Us");
			$tooltip.html(basePrice);
		} else if (numberOfTasks === 0) {
			$tooltip.html("");
			$h1.html("0");
		} else {
			$h1.html(`${commaSeparatedNumber(numberOfTasks)} AED`);
			// var cost = costForTasks(numberOfTasks);
			let cost = 300000;

			if (numberOfTasks <= 200) {
				$tooltip.html(`Unit ${(cost / numberOfTasks).toFixed(0)} AED`);
			} else {
				cost = basePrice * numberOfTasks;
				$tooltip.html(`Unit ${basePrice} AED`);
			}

			$cost.html(
				cost === 0
					? "Free"
					: `<sup>تومان</sup> ${commaSeparatedNumber(cost)}`
			);
		}
		numberOfTasks >= 50000
			? $estimator.addClass("contactUs")
			: $estimator.removeClass("contactUs");
	};

	sliderChanged(0.45);

	$(".slider").on("mousedown touchstart", function(e) {
		$(".handle").removeClass("pulsate");
		$("body").css({
			"user-select": "none",
			cursor: "-webkit-grabbing",
			cursor: "-moz-grabbing"
		});

		$(window).on("mousemove touchmove", e => {
			e.preventDefault();

			const posX = e.originalEvent.touches
				? e.originalEvent.touches[0].pageX
				: e.pageX;

			let percent =
				(posX - $(".slider").offset().left) / $(".slider").width();

			percent = Math.max(percent, 0);
			percent = Math.min(percent, 1);
			requestAnimationFrame(() => {
				sliderChanged(2 * percent);
			});
		});
	});

	$(window).on("mouseup touchend", () => {
		$(window).off("mousemove touchmove");
		// $("body").css({
		// 	"user-select": "text",
		// 	cursor: ""
		// });
		$(".handle").addClass("pulsate");
	});

	/**
	 * Contact Form
	 */
	(function contactForm() {
		/**
		 * variables
		 */
		const $el = $("form.requestInvite");
		const $submit = $el.find(".submit");
		const $success = $el.find(".success");
		const data = {};

		/**
		 * Loading
		 * @param  {[type]} state [description]
		 */
		const loading = state => {
			if (state) {
				$submit.addClass("submitting");
			} else {
				$submit.removeClass("submitting");
			}
		};

		function printError(message, input, opts) {
			input.addClass("invalid");
			// input
			// 	.parents(".input-holder")
			// 	.append(`<span id="${opts.id}_hint" class="hint error">${message}</span>`);
		}

		function sendData() {
			const ary = $el.serializeArray();
			const action =
				$el.attr("action") ||
				"https://submit-form.com/nGsJG00BceckpHue75Ls0";
			// "https://usebasin.com/f/94e7886dc51c.json";
			for (let a = 0; a < ary.length; a++) {
				data[ary[a].name] = ary[a].value;
			}
			data.email = data["ایمیل"];
			delete data["ایمیل"];

			postData(action, data)
				.then(data => {
					loading(false);
					$success.addClass("active");

					setTimeout(() => {
						$("form.requestInvite")[0].reset();
						$success.removeClass("active");
					}, 10000);
				})
				.catch(err => {
					// .fail
					loading(false);
				});
		}

		$el.find(".submit").on("click", formValidate);
		$el.on("submit", formValidate);

		$el.find(".input").on("keyup", function() {
			$(this).removeClass("invalid");
		});

		function formValidate(e) {
			e.preventDefault();
			const self = $el;

			let has_error = false;
			self.find(".input").removeClass("invalid");

			// Name validation
			const fname = self.find("#input-firstname");
			const fnameVal = fname.val();
			const fnameOpts = {
				id: "first_name"
			};

			if (fnameVal.length === 0) {
				printError("نام اجباری است.", fname, fnameOpts);
				has_error = true;
			}

			// lastName validation
			const lname = self.find("#input-lastname");
			const lnameVal = lname.val();
			const lnameOpts = {
				id: "last_name"
			};

			if (lnameVal.length === 0) {
				printError("نام اجباری است.", lname, lnameOpts);
				has_error = true;
			}

			// Email validation
			const email = self.find("#input-email");
			const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			const emailVal = email.val();
			const emailOpts = {
				id: "email"
			};

			if (emailVal.length === 0) {
				printError("ایمیل اجباری است", email, emailOpts);
				has_error = true;
			} else {
				if (!emailVal.match(emailReg)) {
					printError("ایمیل وارد شده اشتباه است.", email, emailOpts);
					has_error = true;
				}
			}

			// Mobile validation
			const phone = self.find("#input-tel");
			const phoneVal = phone.val();
			// const mobileReg = /(0|\+98)?([ ]|-|[()]){0,2}9[1|2|3|4]([ ]|-|[()]){0,2}(?:[0-9]([ ]|-|[()]){0,2}){8}/ig;
			const phoneOpts = {
				id: "tel"
			};

			function isANumber(n) {
				const numStr = /^-?(\d+\.?\d*)$|(\d*\.?\d+)$/;
				return numStr.test(n.toString());
			}

			if (phoneVal.length > 0) {
				if (!isANumber(phoneVal)) {
					printError("شماره تماس باید عدد باشد.", phone, phoneOpts);
					has_error = true;
				}
			}

			// Company validation
			const company = self.find("#input-company_name");
			const companyVal = company.val();
			const companyOpts = {
				id: "company_name"
			};

			if (companyVal.length === 0) {
				printError("نام شرکت اجباری است.", company, companyOpts);
				has_error = true;
			}

			// Website validation
			const website = self.find("#input-website");
			const websiteReg = /[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+/i;
			const websiteVal = website.val();
			const websiteOpts = {
				id: "website_name"
			};

			if (websiteVal.length === 0) {
				printError("وب‌سایت اجباری است.", website, websiteOpts);
				has_error = true;
			} else {
				if (!websiteReg.test(websiteVal)) {
					printError(
						"مقدار وارد شده با الگو مطابقت ندارد",
						website,
						websiteOpts
					);
					has_error = true;
				}
			}

			if (has_error == false) {
				sendData();
			}

			return false;
		}

		const postData = (url, data) => {
			loading(true);

			return $.post(
				url,
				data,
				null,
				"json" // dataType must be set to json
			);
			// const promise = $.post({
			// 	url,
			// 	data,
			// 	dataType: "json"
			// });
			// const promise = new Promise((resolve, reject) => {
			// 	setTimeout(() => {
			// 		resolve(data);
			// 	}, 3000);
			// });
			// return promise;
		};
	})();
	/**
	 * End Contact Form
	 */

	// const iframeWin = document.getElementById("__preview").contentWindow;
	const jsEncode = {
		encode(s, k) {
			let enc = "";
			// let str = s.toString();
			// make sure that input is string
			// str = s.toString();

			for (let i = 0; i < s.length; i++) {
				// create block
				const a = s.charCodeAt(i);
				// bitwise XOR
				const b = a ^ k;
				enc = enc + String.fromCharCode(b);
			}
			return enc;
		}
	};
	$("#cta-button").on("click", event => {
		const $cost = $(".cost h1");
		const url = "/google-ad";

		const x = $cost.html().split(" ")[1];
		if (x !== "Us") {
			const price = parseFloat(x.replace(/,/g, "")) * 10;
			window.open(
				`${url}/?p=${jsEncode.encode(price.toString(), 2)}`,
				"_blank"
			);
			// iframeWin.postMessage(
			// 	{
			// 		amount: parseFloat(x.replace(/,/g, "")) * 10
			// 	},
			// 	"*"
			// );
			// $(".comp-modal").removeClass("hide");
		} else {
			alert("برای اطلاعات بیشتر تماس بگیرید");
		}
		event.preventDefault();
	});

	// $(".close-modal").on("click", () => {
	// 	$(".comp-modal").addClass("hide");
	// });

	// /* Outer click to document */
	// $(document).on("click", event => {
	// 	if ($(event.target).closest(".comp-modal").length > 0) {
	// 		$(".comp-modal").addClass("hide");
	// 	}

	// 	// event.preventDefault();
	// 	event.stopPropagation();
	// });
});
