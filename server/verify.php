<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>نتیجه پرداخت</title>
	<style>
		html {
			margin: 0;
			padding: 0;
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
			-moz-osx-font-smoothing: grayscale;
			-moz-font-smoothing: antialiased;
			-webkit-font-smoothing: antialiased
		}

		body {
			padding: 0;
			margin: 0;
			font-family: is;
			direction: rtl;
			font-size: 14px;
			line-height: 26px;
			color: #222;
		}


		@font-face {
			font-family: "is";
			font-weight: 400;
			src: url("iransans/is.woff2") format("woff2");
		}

		@font-face {
			font-family: "is";
			font-weight: 500;
			src: url("iransans/is_medium.woff2") format("woff2");
		}

		.wrap {
			box-sizing: border-box;
			width: 25rem;
			margin: 0 auto;
			max-width: 100%;
			padding: 0 1rem;
		}

		.callback {
			padding: 2rem 0 3rem;
			background-color: #ebecec;
		}

		.callback_header {
			text-align: center;
		}

		/* .callback_header-primary {
			color: #303645 !important;
			font-size: 1.5rem;
			margin-top: 0.5rem;
			margin-bottom: 0.25rem
		} */

		.callback_header-secondary {
			font-size: 1.5rem;
			margin-bottom: 0;
			color: #28bb70;
			font-weight: 500
		}

		.callback_header-secondary.failed {
			color: #f44336;
		}

		.callback-main {
			position: relative;
			max-width: 100%;
			padding: 1.5rem;
			margin: 1.5rem auto;
			color: #555;
			background-color: rgba(255, 255, 255, 0.95);
			border-radius: 3px;
			box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.1), 0 6px 16px rgba(48, 54, 69, 0.1), 0 1px 12px #eee
		}

		.callback-main_price-money {
			margin: 0.5em 0 1rem;
			font-size: 1.5rem;
			font-weight: 500;
			color: #28bb70;
			letter-spacing: -1px;
			word-spacing: -1px;
		}

		.callback-main_price-credit {
			font-size: 1rem;
			margin: 0.5rem 0;
		}

		.callback-main_price-notice {
			font-weight: 500;
			line-height: 1.618 !important;
			border-right: 3px solid #303645;
			color: #303645;
			padding-right: 0.5rem
		}

		.callback-main .btn {
			margin: 1.5rem auto 0 auto;
			display: table;
			padding: 0.5rem 2rem;
			box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15), 0 1px 3px rgba(0, 173, 188, 0.1);
			text-decoration: none;
			background: #28bb70;
			border-radius: 4px;
			color: #fff;
			font-weight: 400;
			font-size: 1rem;
		}

		.callback-main .hero-title {
			margin: 0;
			font-size: 1.125rem;
			font-weight: 500
		}

		.callback-main .holder {
			display: inline-block;
			margin: 0 4px;
			font-weight: 500;
			color: #303645;
			white-space: nowrap;
		}
	</style>
</head>

<body class="callback">

	<div class="wrap">
		<?php
		include_once("functions.php");
		$api = API;
		$token = $_GET['token'];
		$result = json_decode(verify($api, $token));

		if (isset($result->status)) {
			?>
			<header class="callback_header">

				<?php
				if ($_GET['status'] == 0) {
					echo "<h4 class='callback_header-secondary failed'>پرداخت ناموفق</h4>";
					echo "<p class='callback-main_price-credit'>$result->errorMessage</h4>";
				} ?>

				<?php
				if ($_GET['status'] == 1) {
					echo "<h4 class='callback_header-secondary'>پرداخت موفق</h4>";
				} ?>

			</header>
			<?php
			if ($_GET['status'] == 1) {
				echo "
			<section class='callback-main'>
			<div class='callback-main_box'>
				<h2 class='hero-title'>رسید پرداخت آنلاین:</h2>
				<div class='callback-main_price'>
					<h3 class='callback-main_price-money'>$result->amount ریال</h3>
					<p class='callback-main_price-credit'>شماره موبایل:
						<span class='holder'>$result->mobile</span>
					</p>
					<p class='callback-main_price-credit'>شماره فاکتور:
						<span class='holder'>$result->factorNumber</span>
					</p>
					<p class='callback-main_price-notice'>با شما تماس خواهیم گرفت.</p>
				</div><a href='http://upwork.ws' class='btn'>
					برگشت به وب‌سایت
				</a>
			</div>
		</section>";
			}
		} ?>
	</div>



</body>

</html>
