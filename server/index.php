<?php
include_once("functions.php");
$d = new jsEncode();
$api = API;
$amount = $d->encodeString($_GET['a'], SALT);
$mobile = $d->encodeString($_GET['m'], SALT);
$factorNumber = $d->encodeString($_GET['f'], SALT);
$description = DESCRIPTION;
$redirect = REDIRECT;
$result = send($api, $amount, $redirect, $mobile, $factorNumber, $description);
$result = json_decode($result);

if ($result->status) {
	$go = "https://pay.ir/pg/$result->token";

	http_response_code(200);
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	// echo json_encode(['mobile' => $mobile, 'amount' => $amount]);
	echo json_encode(['pg' => $go]);
	// echo json_encode($result);
} else {
	http_response_code(500);
	echo $result->errorMessage;
}
