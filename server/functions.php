<?php

define('ROOT_URL', 'https://pay.ir/pg/');
define('API', '00000');
define('SALT', 3.14159265359);
define('REDIRECT', 'http://api.upwork.ws/verify');
define('DESCRIPTION', 'پرداخت فاکتور');

function send($api = API, $amount, $redirect = REDIRECT, $mobile = null, $factorNumber = null, $description = DESCRIPTION)
{
	$params = [
		'api'          => $api,
		'amount'       => (int)$amount,
		'redirect'     => $redirect,
		'mobile'       => $mobile,
		'factorNumber' => $factorNumber,
		'description'  => $description,
	];
	$out = array_values($params);

	return curl_post(
		ROOT_URL . 'send/?api=test',
		"amount=$out[1]" . "&redirect=$out[2]" . "&mobile=$out[3]" . "&factorNumber=$out[4]" . "&description=$out[5]"
		// "api=" . json_encode($out[0]) . "&amount=" . json_encode($out[1]) . "&redirect=$out[2]" . "&mobile=" . json_encode($out[3]) . "&factorNumber=" . json_encode($out[4]) . "&description=" . json_encode($out[5])
	);
}

function verify($api, $token)
{
	$params = [
		'api' 	=> API,
		'token' => $token,
	];
	$out = array_values($params);

	return curl_post(ROOT_URL . 'verify/?api=test', "token=$out[1]");
	// return curl_post(ROOT_URL . 'verify/?api=test', "api=" . json_encode($out[0]) . "&token=" . json_encode($out[1]));
}

function curl_post($url, $params)
{
	$curl = curl_init();


	// var_dump("amount=" . json_encode($out[1]) . "redirect=" . json_encode($out[2]));
	// var_dump("amount=" . json_encode($out[1]) . "&redirect=" . json_encode($out[2]) . "&mobile=" . json_encode($out[3]) . "&factorNumber=" . json_encode($out[4]) . "&description=" . json_encode($out[5]));

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $params,
		CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded"
		),
	));

	$response = curl_exec($curl);
	// $err = curl_error($curl);

	curl_close($curl);

	return $response;
}

class jsEncode
{

	/**
	 * Encodes or decodes string according to key
	 *
	 * @access public
	 * @param mixed $str
	 * @param mixed $decodeKey
	 * @return string
	 */

	public function encodeString($str, $decodeKey)
	{
		$result = "";
		for ($i = 0; $i < strlen($str); $i++) {
			$a = $this->_getCharcode($str, $i);
			$b = $a ^ $decodeKey;
			$result .= $this->_fromCharCode($b);
		}

		return $result;
	}

	/**
	 * PHP replacement for JavaScript charCodeAt.
	 *
	 * @access private
	 * @param mixed $str
	 * @param mixed $i
	 * @return string
	 */
	private function _getCharcode($str, $i)
	{
		return $this->_uniord(substr($str, $i, 1));
	}

	/**
	 * Gets character from code.
	 *
	 * @access private
	 * @return string
	 */
	private function _fromCharCode()
	{
		$output = '';
		$chars = func_get_args();
		foreach ($chars as $char) {
			$output .= chr((int)$char);
		}
		return $output;
	}


	/**
	 * Multi byte ord function.
	 *
	 * @access private
	 * @param mixed $c
	 * @return mixed
	 */
	private function _uniord($c)
	{
		$h = ord($c{
			0});
		if ($h <= 0x7F) {
			return $h;
		} else if ($h < 0xC2) {
			return false;
		} else if ($h <= 0xDF) {
			return ($h & 0x1F) << 6 | (ord($c{
				1}) & 0x3F);
		} else if ($h <= 0xEF) {
			return ($h & 0x0F) << 12 | (ord($c{
				1}) & 0x3F) << 6 | (ord($c{
				2}) & 0x3F);
		} else if ($h <= 0xF4) {
			return ($h & 0x0F) << 18 | (ord($c{
				1}) & 0x3F) << 12 | (ord($c{
				2}) & 0x3F) << 6 | (ord($c{
				3}) & 0x3F);
		} else {
			return false;
		}
	}
}
