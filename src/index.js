// import 'promise-polyfill';
// import 'isomorphic-fetch';
import { h, render } from "preact";
import { getQueryVariable } from "./lib/utils";
// import "./styles/kit/framy.css";
import "./styles/styles.scss";

let root;
function init() {
	let App = require("./components/app").default;
	root = render(<App />, document.body, root);
}

// register ServiceWorker via OfflinePlugin, for prod only:
// if (process.env.NODE_ENV === "production") {
// 	require("./pwa");
// }

// in development, set up HMR:
if (module.hot) {
	//require('preact/devtools');   // turn this on if you want to enable React DevTools!
	module.hot.accept("./components/app", () => requestAnimationFrame(init));
}

const amount = getQueryVariable("p");
if (amount) {
	init();
}

// (function inIframe() {
// 	if (window.self !== window.top) {
// 		init();
// 	}
// 	// try {
// 	// 	// in Iframe
// 	// 	return window.self !== window.top;
// 	// } catch (e) {
// 	// 	return true;
// 	// }
// })();
