export function debounce(callback, wait, context = this) {
	let timeout = null;
	let callbackArgs = null;

	const later = () => callback.apply(context, callbackArgs);

	return function() {
		callbackArgs = arguments;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
	};
}

export const jsEncode = {
	encode(s, k) {
		let enc = "";
		// let str = s.toString();
		// make sure that input is string
		// str = s.toString();

		for (let i = 0; i < s.length; i++) {
			// create block
			const a = s.charCodeAt(i);
			// bitwise XOR
			const b = a ^ k;
			enc = enc + String.fromCharCode(b);
		}
		return enc;
	}
};

export const factorNumberGen = () =>
	Math.random()
		.toString(9)
		.substr(2, 9);

export function commaSeparatedNumber(val) {
	val = val.toString().replace(/,/g, "");
	for (
		var valSplit = val.split(".");
		/(\d+)(\d{3})/.test(valSplit[0].toString());

	)
		valSplit[0] = valSplit[0].toString().replace(/(\d+)(\d{3})/, "$1,$2");
	return (val =
		2 == valSplit.length ? `${valSplit[0]}.${valSplit[1]}` : valSplit[0]);
}

export function getQueryVariable(variable) {
	const query = window.location.search.substring(1);
	const vars = query.split("&");
	for (let i = 0; i < vars.length; i++) {
		let pair = vars[i].split("=");
		if (pair[0] === variable) {
			return pair[1];
		}
	}
	return false;
}

export const validation = {
	isEmailAddress: str => {
		const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		// const pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		return pattern.test(str); // returns a boolean
	},
	isNotEmpty: str => {
		const pattern = /\S+/;
		return pattern.test(str); // returns a boolean
	},
	isNumber: str => {
		const pattern = /^\d+$/;
		return pattern.test(str); // returns a boolean
	},
	isSame: (str1, str2) => {
		return str1 === str2;
	}
};
