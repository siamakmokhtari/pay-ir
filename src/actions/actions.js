import request from "axios";
import { jsEncode } from "../lib/utils";

export function postMeta(meta) {
	const options = {
		method: "GET",
		url: "http://api.upwork.ws/index",
		params: {
			a: meta.amount && jsEncode.encode(meta.amount, 3.14159265359),
			m: meta.mobile && jsEncode.encode(meta.mobile, 3.14159265359),
			f:
				meta.factorNumber &&
				jsEncode.encode(meta.factorNumber, 3.14159265359)
		}
	};
	request(options)
		// .then(data => console.log(data.data.pg))
		.then(data => (window.location.href = data.data.pg))
		// .then(data => (top.window.location.href = data.data.pg))
		.catch(err => console.log(err));
}

export function postInvoice(params) {
	// console.log(params);
	return fetch("https://submit-form.com/2j8pQpzAu9XaPMewlrz6i", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json"
		},
		body: JSON.stringify(params)
	});
	// return request("https://submit-form.com/EGV5C-PgemYPnSVR3KMxx", {
	// 	method: "POST",
	// 	headers: {
	// 		"Content-Type": "application/json",
	// 		Accept: "application/json"
	// 	},
	// 	body: JSON.stringify(params),
	// 	dataType: "json"
	// });
	// return request({
	// 	// url: "https://usebasin.com/f/94e7886dc51c.json",
	// 	url: "https://usebasin.com/f/ceb6fd20ca41.json",
	// 	method: "POST",
	// 	data: params,
	// 	dataType: "json"
	// });
}
