import { h, Component } from "preact";
import { validation } from "../lib/utils";
import cx from "classnames";

export default class Input extends Component {
	constructor(props) {
		super(props);
		this.onChange = this.onChange.bind(this);
	}

	state = {
		value: this.props.value,
		length:
			this.props.maxLengthLimit -
			((this.props.value && this.props.value.length) || 0),
		hasError: ""
	};

	componentDidMount() {
		const { value, name } = this.props;
		if (value) {
			this.onChange({
				target: {
					name,
					value
				}
			});
		}
	}

	onChange(e) {
		const {
			maxLengthLimit,
			onChange,
			required,
			name,
			onError
		} = this.props;
		const element = e.target;
		const value = element.value;
		const chars = value.length;
		const remainingChar = maxLengthLimit ? maxLengthLimit - chars : 0;

		this.setState({
			length: remainingChar,
			value
		});

		if (required) {
			if (chars === 0) {
				onError(name, "فیلد اجباری است");
				onChange({
					target: {
						name,
						value: null
					}
				});
				this.setState({
					hasError: "فیلد اجباری است"
				});
			} else {
				onError(name, null);
				onChange(e);
				this.setState({
					hasError: ""
				});
			}

			if (name === "phone") {
				const isNumber = validation.isNumber(value);
				// console.log(isNumber);
				if (!isNumber) {
					onError(name, "فیلد فقط می‌تواند عدد باشد");
					this.setState({
						hasError: "فیلد فقط می‌تواند عدد باشد"
					});
				}
			}
			if (name === "email") {
				const isNumber = validation.isEmailAddress(value);
				// console.log(isNumber);
				if (!isNumber) {
					onError(name, "آدرس ایمیل، eg@gmail.com");
					this.setState({
						hasError: "آدرس ایمیل، eg@gmail.com"
					});
				}
			}

			return;
		}

		if (remainingChar >= 0) {
			onChange(e);
		}
	}

	render() {
		const {
			Component = "input",
			name,
			type = "text",
			fa_title,
			en_title,
			placeholder,
			pattern,
			dir,
			maxLengthLimit
		} = this.props;
		const { length, value, hasError } = this.state;

		return (
			<div class="form-element">
				{fa_title && (
					<label for={name}>
						{fa_title}
						{hasError && (
							<span class="alert error">{hasError}</span>
						)}

						{en_title && <code>{en_title}</code>}
					</label>
				)}

				{/* {Component === "select" && (
					<select
						type="text"
						class="form-field"
						name={name}
						id={name}
						value={value}
						onChange={this.onChange}
						onBlur={this.onChange}
						dir={dir}
						placeholder={`${fa_title} را انتخاب کنید`}
					>
						{this.props.options &&
							Object.keys(this.props.options).map(opt => (
								<option value={opt}>{opt}</option>
							))}
					</select>
				)} */}

				{(maxLengthLimit && (
					<div class="form-group">
						<span
							class={cx("counter", {
								ok: length >= 0,
								oh: length < 0
							})}
						>
							{length}
						</span>
						<Component
							type={type}
							class={cx("form-field", { error: hasError })}
							name={name}
							id={name}
							value={value}
							pattern={pattern}
							placeholder={placeholder}
							onKeyUp={this.onChange}
							onBlur={this.onChange}
							dir={dir}
							// onKeyUp={debounce(this.onChange, 200)}
						/>
					</div>
				)) || (
					<Component
						type={type}
						class={cx("form-field", { error: hasError })}
						name={name}
						id={name}
						value={value}
						pattern={pattern}
						placeholder={placeholder}
						onKeyUp={this.onChange}
						onBlur={this.onChange}
						dir={dir}
						// onKeyUp={debounce(this.onChange, 200)}
					/>
				)}
			</div>
		);
	}
}
