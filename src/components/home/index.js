import { h, Component } from "preact";
import cx from "classnames";
import style from "./index.scss";
import { getTeamPlayers, postFixtureArrangement } from "../../actions/fixture";
import Player from "../player";

const dic = {
	Forward: "مهاجم",
	Midfielder: "هافبک",
	Defender: "دفاع",
	Goalkeeper: "دروازه بان"
};

export default class App extends Component {
	state = {
		attrs: {
			token: "",
			fixtureId: 8,
			teamId: 1
		},
		// zones: ["Forward", "Midfielder", "Defender", "Goalkeeper"],
		loaded: false,
		isLoading: false,
		showModalWithZone: false,
		players: [
			{
				id: 0,
				player: {},
				zone: "Goalkeeper"
			},
			{
				id: 1,
				player: {},
				zone: "Defender"
			},
			{
				id: 2,
				player: {},
				zone: "Defender"
			},
			{
				id: 3,
				player: {},
				zone: "Defender"
			},
			{
				id: 4,
				player: {},
				zone: "Defender"
			},
			{
				id: 5,
				player: {},
				zone: "Midfielder"
			},
			{
				id: 6,
				player: {},
				zone: "Midfielder"
			},
			{
				id: 7,
				player: {},
				zone: "Midfielder"
			},
			{
				id: 8,
				player: {},
				zone: "Forward"
			},
			{
				id: 9,
				player: {},
				zone: "Forward"
			},
			{
				id: 10,
				player: {},
				zone: "Forward"
			}
		],
		selected: [],

		team: [],
		submited: {}
		// team: [
		// 	{
		// 		id: 1,
		// 		name: "اشکان دژاگه",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/a-dejagah.png",
		// 		position: "Midfielder",
		// 		playerNumber: 21,
		// 		age: 32,
		// 		seasonGamePlayed: 20
		// 	},
		// 	{
		// 		id: 4,
		// 		name: "محسن فروزان",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/mohsenFrozan.png",
		// 		position: "Goalkeeper",
		// 		playerNumber: 1,
		// 		age: 30,
		// 		seasonGamePlayed: 21
		// 	},
		// 	{
		// 		id: 5,
		// 		name: "مهدی محمدیان",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/mehdiMohamadian.png",
		// 		position: "Goalkeeper",
		// 		playerNumber: 33,
		// 		age: 20,
		// 		seasonGamePlayed: 22
		// 	},
		// 	{
		// 		id: 6,
		// 		name: "محمرضا مهدی زاده",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/mohamadRezaMehdiZadeh.png",
		// 		position: "Defender",
		// 		playerNumber: 16,
		// 		age: 23,
		// 		seasonGamePlayed: 23
		// 	},
		// 	{
		// 		id: 7,
		// 		name: "علی اسماعیلی",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/aliEsmaeili.png",
		// 		position: "Defender",
		// 		playerNumber: 40,
		// 		age: 23,
		// 		seasonGamePlayed: 2
		// 	},
		// 	{
		// 		id: 8,
		// 		name: "رضا شربتی",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/rezaSharbati.png",
		// 		position: "Defender",
		// 		playerNumber: 3,
		// 		age: 23,
		// 		seasonGamePlayed: 10
		// 	},
		// 	{
		// 		id: 9,
		// 		name: "علی عبدالله زاده",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/aliAbdolahZadeh.png",
		// 		position: "Defender",
		// 		playerNumber: 5,
		// 		age: 25,
		// 		seasonGamePlayed: 24
		// 	},
		// 	{
		// 		id: 10,
		// 		name: "محمد طیبی",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/mohammadTayebi.png",
		// 		position: "Defender",
		// 		playerNumber: 2,
		// 		age: 31,
		// 		seasonGamePlayed: 20
		// 	},
		// 	{
		// 		id: 11,
		// 		name: "مهدی مهدی پور",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/mehdiMehdipour.png",
		// 		position: "Midfielder",
		// 		playerNumber: 9,
		// 		age: 24,
		// 		seasonGamePlayed: 15
		// 	},
		// 	{
		// 		id: 12,
		// 		name: "دانیال اسماعیلی فر",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/danyalEsmaeiFar.png",
		// 		position: "Midfielder",
		// 		playerNumber: 8,
		// 		age: 25,
		// 		seasonGamePlayed: 25
		// 	},
		// 	{
		// 		id: 13,
		// 		name: "علیرضا نقی زاده",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/alirezaNaghizadeh.png",
		// 		position: "Midfielder",
		// 		playerNumber: 6,
		// 		age: 25,
		// 		seasonGamePlayed: 16
		// 	},
		// 	{
		// 		id: 14,
		// 		name: "مسعود شجاعی",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/masoodShojaei.png",
		// 		position: "Midfielder",
		// 		playerNumber: 7,
		// 		age: 34,
		// 		seasonGamePlayed: 5
		// 	},
		// 	{
		// 		id: 15,
		// 		name: "یوکیا سوگیتا",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/yokiaSogita.png",
		// 		position: "Midfielder",
		// 		playerNumber: 24,
		// 		age: 25,
		// 		seasonGamePlayed: 10
		// 	},
		// 	{
		// 		id: 16,
		// 		name: "احسان پهلوان",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/ehsanPahlavan.png",
		// 		position: "Forward",
		// 		playerNumber: 10,
		// 		age: 24,
		// 		seasonGamePlayed: 10
		// 	},
		// 	{
		// 		id: 17,
		// 		name: "لی هری اروین",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/liHeriErvin.png",
		// 		position: "Forward",
		// 		playerNumber: 19,
		// 		age: 24,
		// 		seasonGamePlayed: 14
		// 	},
		// 	{
		// 		id: 18,
		// 		name: "آنتونی استوکس",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/antoniEstoks.png",
		// 		position: "Forward",
		// 		playerNumber: 28,
		// 		age: 30,
		// 		seasonGamePlayed: 26
		// 	},
		// 	{
		// 		id: 2,
		// 		name: "علی طاهران",
		// 		img:
		// 			"http://cdn.omidanalyzer.com/tractor/players/a-taheran.png",
		// 		position: "Midfielder",
		// 		playerNumber: 8,
		// 		age: 19,
		// 		seasonGamePlayed: 15
		// 	},
		// 	{
		// 		id: 3,
		// 		name: "محمدرضا آزادی",
		// 		img: "http://cdn.omidanalyzer.com/tractor/players/m-azadi.png",
		// 		position: "Forward",
		// 		playerNumber: 19,
		// 		age: 19,
		// 		seasonGamePlayed: 5
		// 	}
		// ]
	};

	async componentDidMount() {
		window.addEventListener("message", this.getterOptions.bind(this));
		// document.addEventListener("message", this.getterOptions.bind(this));
		document.addEventListener("click", this.handleOutsideClick.bind(this));
		const team = await getTeamPlayers(this.state.attrs.teamId);
		this.setState({
			team
		});
		setTimeout(() => {
			this.setState({
				loaded: true
			});
		}, 500);
	}

	componentWillUnmount() {
		document.removeEventListener(
			"click",
			this.handleOutsideClick.bind(this)
		);
		window.removeEventListener("message", this.getterOptions.bind(this));
	}

	getterOptions(event) {
		// alert(JSON.stringify(event.data));
		if (event.data.hasOwnProperty("token")) {
			this.setState({
				attrs: event.data
			});

			localStorage.setItem("token", event.data.token);
		}
	}

	clickOnZone(player) {
		this.setState({
			showModalWithZone: {
				zone: player.zone,
				i: player.id
			}
		});
	}

	async onSubmitFixture() {
		const { selected, attrs } = this.state;
		this.setState({
			isLoading: true
		});
		// const arrangementPredictItems = [];
		// players.filter(p => {
		// 	if (p.player && p.player.id) {
		// 		arrangementPredictItems.push({
		// 			position: p.id,
		// 			playerId: p.player.id
		// 		});
		// 	}
		// });

		const p = await postFixtureArrangement({
			arrangementPredictItems: selected,
			fixtureId: attrs.fixtureId,
			teamId: attrs.teamId
		});

		this.setState({
			submited: p,
			isLoading: false
		});

		console.log({ p });
	}

	selectPlayer(i, player) {
		// console.log({ i, player });
		const { players, selected } = this.state;
		const find = players.findIndex(p => p.id === i);
		players[find] = {
			...players[find],
			player
		};
		selected[i] = player.id;

		this.setState({
			showModalWithZone: false,
			players,
			selected
		});
	}

	handleOutsideClick(event) {
		if (
			!this.$outer.contains(event.target) &&
			!this.$selectPlayer.contains(event.target)
		) {
			this.setState({
				showModalWithZone: false
			});
		}
	}

	render() {
		const {
			team,
			showModalWithZone,
			players,
			loaded,
			isLoading,
			selected,
			submited
		} = this.state;
		console.log({ selected });

		return (
			<main class={style.body}>
				<div class={style.wrap}>
					<button
						class={cx(style.submit, {
							[style.submitDisabled]:
								Object.keys(selected).length !== 11,
							[style.submitClicked]: submited.code,
							[style.submitLoaded]: loaded
						})}
						onClick={this.onSubmitFixture.bind(this)}
					>
						{(isLoading && <i class={style.spinner} />) ||
							(submited.response && submited.response) ||
							"ثبت پیش‌بینی"}
					</button>
					<div
						class={cx(style.outer, {
							[style.active]: showModalWithZone,
							[style.loaded]: loaded,
							loaded
						})}
						ref={$outer => {
							this.$outer = $outer;
						}}
					>
						<div class={style.inner}>
							{players.map(i => (
								<figure
									class={`touchable ${style.touchable} ${
										style["z-" + i.id]
									}`}
									onClick={this.clickOnZone.bind(this, i)}
								>
									<Player player={i} />
								</figure>
							))}
						</div>
					</div>

					<div
						class={cx(style.selectPlayer, {
							[style.active]: showModalWithZone
						})}
						// data-modal={true}
						ref={$selectPlayer => {
							this.$selectPlayer = $selectPlayer;
						}}
					>
						<div class={style.wrap}>
							<h5 class={style.title}>
								انتخاب {dic[showModalWithZone.zone]}
							</h5>
							<div class={style.players}>
								{team &&
									team
										.filter(
											p =>
												p.position ===
												showModalWithZone.zone
										)
										.map(i => {
											return (
												<Player
													onClick={this.selectPlayer.bind(
														this,
														showModalWithZone.i,
														i
													)}
													player={{ player: i }}
													className={cx({
														disabled: selected.find(
															x => x === i.id
														)
													})}
												/>
											);
										})}
							</div>
						</div>
					</div>
				</div>
			</main>
		);
	}
}
