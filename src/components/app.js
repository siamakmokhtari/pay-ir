/* eslint-disable react/jsx-no-bind */
import { h, Component } from "preact";
import SlideToggle from "react-slide-toggle";
import cx from "classnames";

import { postMeta, postInvoice } from "../actions/actions";
import {
	factorNumberGen,
	commaSeparatedNumber,
	getQueryVariable,
	jsEncode
} from "../lib/utils";
import Input from "./input";
import Preview from "./preview";

const _headers = {
	Amenities: true,
	Brands: false,
	Courses: false,
	"Degree programs": false,
	Destinations: false,
	"Featured hotels": false,
	"Insurance coverage": false,
	Models: false,
	Neighborhoods: false,
	"Service catalog": false,
	Shows: false,
	Styles: false,
	Types: false
};

export default class App extends Component {
	constructor(props) {
		super(props);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleToggle = this.handleToggle.bind(this);
		this.handleUser = this.handleUser.bind(this);
		this.handleErrors = this.handleErrors.bind(this);
		this.submit = this.submit.bind(this);
	}
	state = {
		amount: "",
		factorNumber: factorNumberGen(),
		inputs: {
			slice_1: "ads",
			slice_2: "google-ads",
			origin: "http://upwork.ws/",
			header: "Amenities"
		},
		toggles: {},
		user: {},
		errors: { name: "name", phone: "phone", email: "email" },
		isLoading: false
	};

	handleToggle(e) {
		this.setState({
			toggles: {
				...this.state.toggles,
				[e.target.name]:
					!this.state.toggles[e.target.name] || e.target.checked
			}
		});
	}

	handleInputChange(e) {
		this.setState({
			inputs: {
				...this.state.inputs,
				[e.target.name]: e.target.value
			}
		});

		if (e.target.name === "final_url") {
			this.setState({
				inputs: {
					...this.state.inputs,
					origin: e.target.value
				}
			});
		}
	}

	handleUser(e) {
		this.setState({
			user: {
				...this.state.user,
				[e.target.name]: e.target.value
			}
		});
	}

	handleErrors(input, error) {
		return this.setState({
			errors: {
				...this.state.errors,
				[input]: error
			}
			// hasError: true
		});
	}

	componentDidMount() {
		const amount = getQueryVariable("p");
		const x = jsEncode.encode(amount, 2);

		if (x) {
			this.setState({
				amount: x
			});
		} else {
		}
	}
	// componentDidMount() {
	// 	window.addEventListener("message", this.getterOptions.bind(this));
	// }

	// componentWillUnmount() {
	// 	window.removeEventListener("message", this.getterOptions.bind(this));
	// }

	// getterOptions(event) {
	// 	if (event.data.hasOwnProperty("amount")) {
	// 		this.setState({
	// 			amount: event.data.amount
	// 		});
	// 	}
	// }

	submit() {
		const { inputs, user, factorNumber, amount } = this.state;
		this.setState({
			isLoading: true
		});
		if (user.name && user.phone && user.email) {
			postInvoice({
				"مبلغ فاکتور": amount,
				"شماره فاکتور": factorNumber,
				...user,
				...inputs
			}).then(() => {
				postMeta({
					amount: amount.toString(),
					mobile: user.phone,
					factorNumber
				});
				this.setState({
					isLoading: false
				});
			});
		} else {
			// console.log("Has error");
		}
	}

	render() {
		// console.log(this.state);
		const { inputs, errors, amount, isLoading } = this.state;
		return (
			<div id="app">
				<form className="user--account">
					<div className="container-fluid">
						<div class="columns">
							<div class="md-4">
								<Input
									name="name"
									fa_title="نام و نام خانوادگی"
									en_title="Name"
									placeholder="نام و نام خانوادگی وارد کنید..."
									required
									onError={this.handleErrors}
									onChange={this.handleUser}
								/>
							</div>
							<div class="md-4">
								<Input
									name="phone"
									fa_title="شماره تماس"
									en_title="Phone"
									placeholder="شماره تماس وارد کنید..."
									required
									pattern="\d*"
									onError={this.handleErrors}
									onChange={this.handleUser}
								/>
							</div>
							<div class="md-4">
								<Input
									name="email"
									fa_title="پست‌الکترونیک"
									en_title="Email"
									type="email"
									placeholder="پست‌الکترونیک وارد کنید..."
									required
									onError={this.handleErrors}
									onChange={this.handleUser}
								/>
							</div>
						</div>
					</div>
				</form>
				<div class="columns min-h">
					<div class="md-6 has-padd">
						<div className="container-fluid">
							<h1>پیش‌نمایش متن تبلیغات گوگل ادوردز</h1>
							<p>
								این ابزار به شما کمک خواهد کرد که متن‌های مورد
								نظر خود برای تبلیغات گوگل ادوردز را آماده کنید و
								آنها را با دیگران به اشتراک بگذارید.
							</p>
							<form>
								<Input
									name="final_url"
									fa_title="آدرس سایت"
									en_title="Final URL"
									value="http://upwork.ws/"
									dir={"ltr"}
									// maxLengthLimit={40}
									onChange={this.handleInputChange}
								/>

								<Input
									name="haedline_1"
									fa_title="عنوان اول"
									en_title="Headline 1"
									value="تبلیغات موثر در گوگل"
									// dir={"ltr"}
									maxLengthLimit={30}
									onChange={this.handleInputChange}
								/>
								<Input
									name="haedline_2"
									fa_title="عنوان دوم"
									en_title="Headline 2"
									value="ما نتایجی بهتر رقم می‌زنیم"
									maxLengthLimit={30}
									onChange={this.handleInputChange}
								/>
								<Input
									name="haedline_3"
									fa_title="عنوان سوم"
									en_title="Headline 3"
									value="افزایش باور نکردنی فروش"
									maxLengthLimit={30}
									onChange={this.handleInputChange}
								/>

								<div class="form-element">
									<label for="prefix">
										مسیر سایت
										<code>Path</code>
									</label>
									<div class="form-group">
										<span>{inputs.origin}</span>
										<input
											class="form-field"
											name="slice_1"
											type="text"
											value={inputs.slice_1}
											dir="ltr"
											onKeyUp={this.handleInputChange}
											onBlur={this.handleInputChange}
										/>
										<span>/</span>
										<input
											class="form-field"
											name="slice_2"
											type="text"
											value={inputs.slice_2}
											dir="ltr"
											onKeyUp={this.handleInputChange}
											onBlur={this.handleInputChange}
										/>
									</div>
								</div>

								<Input
									Component={"textarea"}
									name="description_1"
									fa_title="توضیحات اول"
									en_title="Description 1"
									value="فروش شما را چند برابر خواهیم کرد، اما... هزینه های تبلیغ شما را کاهش می دهیم"
									maxLengthLimit={90}
									onChange={this.handleInputChange}
								/>

								<Input
									Component={"textarea"}
									name="description_2"
									fa_title="توضیحات دوم"
									en_title="Description 2"
									value="با تجربه و تخصص ما به هدفنمدترین شکل ممکن در گوگل تبلیغ کنید"
									maxLengthLimit={90}
									onChange={this.handleInputChange}
								/>

								<div class="form-element">
									<Collapsed
										name="option_1"
										title="اضافه کردن لینک‌های سایت"
										onChange={this.handleToggle}
									>
										<div className="columns">
											<div className="sm-6">
												<Input
													name="sitelink_1"
													fa_title="سایت لینک ۱"
													en_title="Sitelink 1"
													value="تبلیغ گوگل چیست؟"
													maxLengthLimit={25}
													onChange={
														this.handleInputChange
													}
												/>
											</div>
											<div className="sm-6">
												<Input
													name="sitelink_url_1"
													fa_title="آدرس لینک ۱"
													en_title="Sitelink 1 URL"
													value="#"
													dir={"ltr"}
													onChange={
														this.handleInputChange
													}
												/>
											</div>
											<div className="sm-6">
												<Input
													name="sitelink_2"
													fa_title="سایت لینک ۲"
													en_title="Sitelink 2"
													value="چرا تبلیغ با ؟"
													maxLengthLimit={25}
													onChange={
														this.handleInputChange
													}
												/>
											</div>
											<div className="sm-6">
												<Input
													name="sitelink_url_2"
													fa_title="آدرس لینک ۲"
													en_title="Sitelink 2 URL"
													value="#"
													dir={"ltr"}
													onChange={
														this.handleInputChange
													}
												/>
											</div>
											<div className="sm-6">
												<Input
													name="sitelink_3"
													fa_title="سایت لینک ۳"
													en_title="Sitelink 3"
													value="تعرفه های تبلیغات"
													maxLengthLimit={25}
													onChange={
														this.handleInputChange
													}
												/>
											</div>
											<div className="sm-6">
												<Input
													name="sitelink_url_3"
													fa_title="آدرس لینک ۳"
													en_title="Sitelink 3 URL"
													value="#"
													dir={"ltr"}
													onChange={
														this.handleInputChange
													}
												/>
											</div>
											<div className="sm-6">
												<Input
													name="sitelink_4"
													fa_title="سایت لینک ۴"
													en_title="Sitelink 4"
													value="درخواست مشاوره"
													maxLengthLimit={25}
													onChange={
														this.handleInputChange
													}
												/>
											</div>
											<div className="sm-6">
												<Input
													name="sitelink_url_4"
													fa_title="آدرس لینک ۴"
													en_title="Sitelink 4 URL"
													value="#"
													dir={"ltr"}
													onChange={
														this.handleInputChange
													}
												/>
											</div>
										</div>
									</Collapsed>

									<div className="form-element">
										<Collapsed
											name="option_2"
											title="اضافه کردن مزایای بیشتر"
											onChange={this.handleToggle}
										>
											<Input
												name="callout_1"
												fa_title="مزیت ۱"
												en_title="Callout 1"
												value="پشتیبانی از 9 تا 10 شب"
												maxLengthLimit={25}
												onChange={
													this.handleInputChange
												}
											/>

											<Input
												name="callout_2"
												fa_title="مزیت ۲"
												en_title="Callout 2"
												value="فعالیت مداوم روی تبلیغ"
												maxLengthLimit={25}
												onChange={
													this.handleInputChange
												}
											/>

											<Input
												name="callout_3"
												fa_title="مزیت ۳"
												en_title="Callout 3"
												value="مشاوره تخصصی رایگان"
												maxLengthLimit={25}
												onChange={
													this.handleInputChange
												}
											/>

											<Input
												name="callout_4"
												fa_title="مزیت ۴"
												en_title="Callout 4"
												value="گزارش مستقیم از گوگل"
												maxLengthLimit={25}
												onChange={
													this.handleInputChange
												}
											/>
										</Collapsed>
									</div>

									<div className="form-element">
										<Collapsed
											name="option_3"
											title="اضافه کردن شماره تماس"
											onChange={this.handleToggle}
										>
											<Input
												name="phone_number"
												fa_title="شماره تلفن"
												en_title="Phone Number"
												value="02188003373"
												maxLengthLimit={15}
												onChange={
													this.handleInputChange
												}
											/>
										</Collapsed>
									</div>

									<div className="form-element">
										<Collapsed
											name="option_4"
											title="اضافه کردن توضیحات ساختاری"
											onChange={this.handleToggle}
										>
											<label for="header">
												هدر
												<code>Header</code>
											</label>
											<select
												type="text"
												class="form-field"
												name="header"
												id="header"
												onChange={
													this.handleInputChange
												}
												dir={"ltr"}
											>
												{Object.keys(_headers).map(
													opt => (
														<option
															value={opt}
															selected={
																_headers[opt]
															}
														>
															{opt}
														</option>
													)
												)}
											</select>

											<Input
												name="value_1"
												fa_title="مقدار ۱"
												en_title="Value 1"
												value="راه‌اندازی اکانت ادوردز"
												maxLengthLimit={25}
												onChange={
													this.handleInputChange
												}
											/>

											<Input
												name="value_2"
												fa_title="مقدار ۲"
												en_title="Value 2"
												value="شارژ اکانت ادوردز"
												maxLengthLimit={25}
												onChange={
													this.handleInputChange
												}
											/>

											<Input
												name="value_3"
												fa_title="مقدار ۳"
												en_title="Value 3"
												value="مدیریت تبلیغات گوگل"
												maxLengthLimit={25}
												onChange={
													this.handleInputChange
												}
											/>
										</Collapsed>
									</div>

									<div className="form-element">
										<Collapsed
											name="option_5"
											title="مشخص کردن هدفگیری"
											onChange={this.handleToggle}
										>
											<Input
												name="keywords"
												fa_title="کلمات کلیدی"
												en_title="Keywords"
												Component="textarea"
												value="مثال: تبلیغات گوگل، تبلیغ گوگل، تبلیغات در گوگل"
												onChange={
													this.handleInputChange
												}
											/>

											<Input
												name="schedule"
												fa_title="زمانبندی تبلیغ"
												en_title="Schedule Ad"
												value="مثال: شنبه تا چهارشنبه از ساعت 9 تا 22
												پنجشنبه و جمعه از ساعت 9 تا 17"
												Component="textarea"
												onChange={
													this.handleInputChange
												}
											/>
										</Collapsed>
									</div>
								</div>
							</form>
						</div>
					</div>

					<div class="md-6 col-gray has-padd">
						<div className="sticky">
							<div className="container-fluid">
								<h4>پیش‌نمایش تبلیغ شما در گوگل</h4>
								<Preview data={this.state} />
							</div>
						</div>
					</div>

					<div
						className={cx("action--bar green", {
							disabled:
								errors.name || errors.phone || errors.email,
							df: isLoading
						})}
						onClick={!isLoading && this.submit}
					>
						{/* ثبت درخواست */}
						پرداخت مبلغ: {commaSeparatedNumber(amount / 10)} تومان
					</div>
				</div>
			</div>
		);
	}
}

class Collapsed extends Component {
	render() {
		return (
			<SlideToggle
				duration={500}
				collapsed={true}
				bestPerformance={true}
				render={({ onToggle, setCollapsibleElement, toggleState }) => (
					<div className="my-collapsible">
						<label class="checkbox fancy" for={this.props.name}>
							{/* {console.log(toggleState)} */}
							<input
								type="checkbox"
								name={this.props.name}
								id={this.props.name}
								checked={
									toggleState === "EXPANDING" ||
									toggleState === "EXPANDED"
								}
								onChange={e => {
									onToggle(e);
									this.props.onChange(e);
								}}
							/>
							<div />
							<span>{this.props.title}</span>
						</label>
						<div
							className="my-collapsible__content"
							ref={setCollapsibleElement}
						>
							<div className="my-collapsible__content-inner">
								{this.props.children}
							</div>
						</div>
					</div>
				)}
			/>
		);
	}
}
