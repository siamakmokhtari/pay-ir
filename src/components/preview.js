import { h, Component } from "preact";
import cx from "classnames";

export default class Preview extends Component {
	render() {
		const { data } = this.props;
		const { inputs, toggles } = data;
		// console.log(data);
		return (
			<section>
				<div class="gadp-box">
					<a
						href={inputs.final_url}
						class="gadp-title"
						data-wpel-link="internal"
					>
						<span class="gadp-headline1">{inputs.haedline_1}</span>
						<span class="gadp-title-line gadp-title-line1">
							{" "}
							|{" "}
						</span>
						<span class="gadp-headline2">{inputs.haedline_2}</span>
						<span class="gadp-title-line gadp-title-line2">
							{" "}
							|{" "}
						</span>
						<span class="gadp-headline3">{inputs.haedline_3}</span>
					</a>
					<div class="gadp-url-outer">
						<span class="gadp-url-tag">Ad</span>
						<div class="gadp-url">
							{inputs.origin}
							{inputs.slice_1}/{inputs.slice_2}
						</div>
						<span
							class={cx("gadp-phone", {
								"gadp-hide": !toggles.option_3
							})}
						>
							{inputs.phone_number && inputs.phone_number}
						</span>
					</div>
					<div class="gadp-description">
						<span class="gadp-description1">
							{inputs.description_1}
						</span>{" "}
						<span class="gadp-description2">
							{inputs.description_2}
						</span>
					</div>
					<div
						class={cx("gadp-callouts", {
							"gadp-hide": !toggles.option_2
						})}
					>
						{[1, 2, 3, 4].map(
							(x, i) =>
								inputs[`callout_${x}`] && (
									<span>
										{inputs[`callout_${x}`]}
										{i !== 3 && " · "}
									</span>
								)
						)}
					</div>
					<div
						class={cx("gadp-snippets", {
							"gadp-hide": !toggles.option_4
						})}
					>
						{inputs.header}:{" "}
						{[1, 2, 3].map(
							(x, i) =>
								inputs[`value_${x}`] && (
									<span>
										{inputs[`value_${x}`]}
										{i !== 2 && "، "}
									</span>
								)
						)}
					</div>
					<div
						class={cx("gadp-sitelinks", {
							"gadp-hide": !toggles.option_1
						})}
					>
						{[1, 2, 3, 4].map(
							(x, i) =>
								inputs[`sitelink_${x}`] && (
									<a href={inputs[`sitelink_url_${x}`]}>
										{inputs[`sitelink_${x}`]}
										{i !== 3 && " · "}
									</a>
								)
						)}
					</div>
				</div>
				<div class="gadp-mobile iphone-x">
					<div class="gadp-mobile-inner">
						<i class="gadp-google-logo" />
						<div class="gadp-google-search-holder">
							<div class="clearfix">
								<input type="text" />
								<i class="elegant-icon icon_search" />
							</div>
							<div class="gadp-google-menu">
								<span class="active">All</span>
								<span>Images</span>
								<span>Videos</span>
								<span>News</span>
								<span>Books</span>
							</div>
						</div>
						<div class="gadp-mobile-ad">
							<div class="gadp-mobile-title">
								<a
									href={inputs.final_url}
									class="gadp-title"
									data-wpel-link="internal"
								>
									<span class="gadp-headline1">
										{inputs.haedline_1}
									</span>
									<span class="gadp-title-line gadp-title-line1">
										{" "}
										|{" "}
									</span>
									<span class="gadp-headline2">
										{inputs.haedline_2}
									</span>
									<span class="gadp-title-line gadp-title-line2">
										{" "}
										|{" "}
									</span>
									<span class="gadp-headline3">
										{inputs.haedline_3}
									</span>
								</a>
								<div class="gadp-url-outer">
									<span class="gadp-url-tag">Ad</span>
									<div class="gadp-url">
										{inputs.origin}
										{inputs.slice_1}/{inputs.slice_2}
									</div>
								</div>
							</div>
							<div class="gadp-mobile-content">
								<div class="gadp-description">
									<span class="gadp-description1">
										{inputs.description_1}
									</span>{" "}
									<span class="gadp-description2">
										{inputs.description_2}
									</span>
								</div>
								<div
									class={cx("gadp-callouts", {
										"gadp-hide": !toggles.option_2
									})}
								>
									{[1, 2, 3, 4].map(
										(x, i) =>
											inputs[`callout_${x}`] && (
												<span>
													{inputs[`callout_${x}`]}
													{i !== 3 && " · "}
												</span>
											)
									)}
								</div>
								<div
									class={cx("gadp-snippets", {
										"gadp-hide": !toggles.option_4
									})}
								>
									{inputs.header}:{" "}
									{[1, 2, 3].map(
										(x, i) =>
											inputs[`value_${x}`] && (
												<span>
													{inputs[`value_${x}`]}
													{i !== 2 && "، "}
												</span>
											)
									)}
								</div>
								<div
									class={cx("gadp-sitelinks", {
										"gadp-hide": !toggles.option_1
									})}
								>
									{[1, 2, 3, 4].map(
										(x, i) =>
											inputs[`sitelink_${x}`] && (
												<a
													href={
														inputs[
															`sitelink_url_${x}`
														]
													}
												>
													{inputs[`sitelink_${x}`]}
													{i !== 3 && " · "}
												</a>
											)
									)}
								</div>
							</div>
							<span
								class={cx("gadp-phone", {
									"gadp-hide": !toggles.option_3
								})}
							>
								{inputs.phone_number && inputs.phone_number}
							</span>
						</div>
					</div>
				</div>
			</section>
		);
	}
}
